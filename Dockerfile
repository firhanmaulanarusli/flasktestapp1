FROM python:3.8

WORKDIR /usr/src/app

RUN apt-get update &&\
    apt-get install -y --no-install-recommends pipenv &&\
    apt-get install -y --no-install-recommends libgl1-mesa-dev &&\
    # apt-get install -y --no-install-recommends tesseract-ocr tesseract-ocr-ind &&\
    apt-get clean &&\ 
    rm -rf /var/lib/apt/lists/*

# COPY Pipfile ./
# COPY Pipfile.lock ./
# RUN pipenv install --system --deploy --ignore-pipfile

# COPY dataset/haarcascade_frontalface_default.xml ./dataset/
COPY settings.py ./
COPY app.py ./
COPY model.pkl ./
COPY requirements.txt ./

RUN pip install -r requirements.txt
EXPOSE 8080

CMD [ "python", "./mainn.py"]

# --------------------------
# FROM python:3.9
# RUN apt-get update -y &&\
#     apt-get install -y python-pip python-dev

# COPY ./requirements.txt /app/requirements.txt

# WORKDIR /app

# RUN pip install -r requirements.txt

# COPY . /app

# ENTRYPOINT [ "python" ]

# CMD ["app.py"]


# FROM python:3.9

# WORKDIR /usr/src/app

# RUN apt-get update &&\
#     apt-get install -y --no-install-recommends pipenv &&\
#     apt-get install -y --no-install-recommends libgl1-mesa-dev &&\
#     apt-get clean &&\ 
#     rm -rf /var/lib/apt/lists/*

# COPY Pipfile ./
# COPY Pipfile.lock ./
# RUN pipenv install --system --deploy --ignore-pipfile

# COPY settings.py ./
# COPY app.py ./

# EXPOSE 8080

# CMD [ "python", "./app.py"]

# -----------------------------
# FROM centos:7

# WORKDIR /usr/src/app

# RUN yum -y update && \
#     yum -y install httpd && \
#     yum -y install --no-install-recommends pipenv &&\
#     yum -y install --no-install-recommends libgl1-mesa-dev &&\
#     yum clean all   && \ 
#     rm -rf /var/lib/apt/lists/*

# COPY Pipfile ./
# COPY Pipfile.lock ./
# RUN pipenv install --system --deploy --ignore-pipfile

# COPY settings.py ./
# COPY mainn.py ./

# EXPOSE 8080

# CMD [ "python", "./app.py"]

# -----------------------------------
# MAINTAINER linuxtechlab
# LABEL Remarks="This is a dockerfile example for Centos system"

# RUN yum -y update && \
#     yum -y install httpd && \
#     yum clean all

# COPY data/httpd.conf /etc/httpd/conf/httpd.conf
# ADD data/html.tar.gz /var/www/html/
# EXPOSE 80
# ENV HOME /root
# WORKDIR /root

# ENTRYPOINT ["ping"]

# CMD ["google.com"]



# -----------------------